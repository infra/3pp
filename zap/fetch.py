#!/usr/bin/env python
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import enum
import json
import os
import sys
import urllib.request

_URL = (
    "https://github.com/project-chip/zap/releases/download/{version}/zap-{platform}.zip"
)

# Occasionally zap will publish a release tag that doesn't include compiled
# binaries. That breaks this script, so we exempt those tags as a workaround.
# fmt: off
_EXCLUDED_TAGS = [
    "v2023.09.21-nightly",
    "v2023.09.22-nightly",
    "v2023.10.09-nightly",
]
# fmt: on


class ZapVersion(enum.Enum):
    RELEASE = enum.auto()
    PRERELEASE = enum.auto()
    FORCED = enum.auto()

    @property
    def version_string(self):
        if self == ZapVersion.FORCED:
            # This can be modified over time if we ever want to force a specific version
            # to be fetched (and then also modify _ZAP_VERSION below)
            return "v2024.04.15-nightly"
        elif self == ZapVersion.RELEASE:
            return json.load(
                urllib.request.urlopen(
                    "https://api.github.com/repos/project-chip/zap/releases/latest"
                )
            )["tag_name"]
            return
        elif self == ZapVersion.PRERELEASE:
            tags = json.load(
                urllib.request.urlopen(
                    "https://api.github.com/repos/project-chip/zap/tags"
                )
            )
            assert len(tags) > 0, "Empty tags list"

            # Filter excluded tags
            filtered_tags = filter(
                lambda tag: (tag["name"] not in _EXCLUDED_TAGS), tags
            )

            # Tags look like "v2023.01.17-nightly", so sorting them will produce a
            # chronological ordering.
            sorted_tag_names = sorted(t["name"] for t in filtered_tags)

            return sorted_tag_names[-1]
        else:
            raise ValueError("Unknown zap version")


# Which zap version to consider "latest".
_ZAP_VERSION = ZapVersion.PRERELEASE


def do_latest():
    """Retrieve current version."""
    print(_ZAP_VERSION.version_string)


def get_download_url(version, platform):
    """Get URL of the given version and platform."""

    platform_name = {
        "linux-amd64": "linux-x64",
        "linux-arm64": "linux-arm64",
        "mac-amd64": "mac-x64",
        "mac-arm64": "mac-arm64",
        "windows-amd64": "win-x64",
    }[platform]

    url = _URL.format(version=version, platform=platform_name)

    manifest = {"url": [url], "ext": ".zip"}
    print(json.dumps(manifest))


def main():
    ap = argparse.ArgumentParser()
    sub = ap.add_subparsers()

    latest = sub.add_parser("latest")
    latest.set_defaults(func=lambda _opts: do_latest())

    download = sub.add_parser("get_url")
    download.set_defaults(
        func=lambda opts: get_download_url(
            os.environ["_3PP_VERSION"], os.environ["_3PP_PLATFORM"]
        )
    )

    opts = ap.parse_args()
    return opts.func(opts)


if __name__ == "__main__":
    sys.exit(main())
