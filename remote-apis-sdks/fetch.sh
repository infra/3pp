#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Using a custom fetch script because repo has no tags (yet).

set -e

function usage() {
  cat <<EOF
Usage: $0 [get_url|latest]
EOF
}

readonly _REPO="https://fuchsia.googlesource.com/third_party/github.com/bazelbuild/remote-apis-sdks"

# There are no release or tags.
# Manually bump these as needed:
readonly _COMMIT_ID="6f33c7354be3cdbae93e7fbfbcc5bd67f5f6bc53"
readonly _REV=0

function get_url() {
  # There are no releases to download.
  # Grab a snapshot of the source.
cat <<EOF
{
  "url": [ "$_REPO/archive/$_COMMIT_ID.zip" ],
  "ext": ".zip"
}
EOF
}

function latest() {
  # There is no version information.
  echo "$_REV"
}

if [[ "$#" -lt 1 ]]; then
  usage
  exit 1
fi

subcommand="$1"
case "$subcommand" in
  get_url ) get_url ;;
  latest ) latest ;;
  * ) usage ; exit 1;;
esac

