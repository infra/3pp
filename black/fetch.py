#!/usr/bin/env python
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import json
import os
import sys
import urllib.request


def do_latest():
    metadata_url = "https://api.github.com/repos/psf/black/releases/latest"
    resp = json.load(urllib.request.urlopen(metadata_url))
    print(resp["tag_name"])


def get_download_url(version, platform):
    os_name = platform.split("-")[0].replace("mac", "macos")
    ext = ".exe" if platform.startswith("windows") else ""
    download_url = "https://github.com/psf/black/releases/download/%s/black_%s%s" % (
        version,
        os_name,
        ext,
    )

    partial_manifest = {
        "url": [download_url],
        "ext": ext,
    }
    print(json.dumps(partial_manifest))


def main():
    ap = argparse.ArgumentParser()

    sub = ap.add_subparsers()

    latest = sub.add_parser("latest")
    latest.set_defaults(func=lambda _opts: do_latest())

    download = sub.add_parser("get_url")
    download.set_defaults(
        func=lambda opts: get_download_url(
            os.environ["_3PP_VERSION"], os.environ["_3PP_PLATFORM"]
        )
    )

    opts = ap.parse_args()
    return opts.func(opts)


if __name__ == "__main__":
    sys.exit(main())
