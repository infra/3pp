#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e
set -x
set -o pipefail

PREFIX="$1"
DEPS="$2"

./configure

make -j $(nproc) LDFLAGS="-all-static" hexdump
cp hexdump "$PREFIX"
