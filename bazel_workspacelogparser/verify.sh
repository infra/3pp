#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -exo pipefail

# This runs in a new empty directory, unpack the CIPD package to verify
# its content and that it runs properly.
unzip -q "$1"

# Dump current directory content for debugging
ls -l .

PROGRAM=bazel_workspacelogparser.jar
if [[ ! -f "${PROGRAM}" ]]; then
  echo >&2 "Missing file: ${PROGRAM}"
  exit 1
fi

# If _BAZEL_BIN is defined, or `bazel` is in the path, use it to
# verify that the tool can be invoked with the embedded JRE.
# This is only useful for local testing, since the 3pp recipe
# will run this script without a Bazel install.
if [[ -z "${_BAZEL_BIN}" ]]; then
  _BAZEL_BIN=$(which bazel 2>/dev/null || echo "")
fi

if [[ -n "${_BAZEL_BIN}" ]]; then
  echo "Checking that the parser can be invoked with Bazel's embedded JRE"
  echo "Bazel binary: ${_BAZEL_BIN}"

  # `bazel info install_base` requires to be in a Bazel workspace.
  touch WORKSPACE

  # Use the Bazel embedded JDK to run the JAR file.
  INSTALL_BASE=$("${_BAZEL_BIN}" info install_base)
  EMBEDDED_JAVA="${INSTALL_BASE}/embedded_tools/jdk/bin/java"
  if [[ ! -f "${EMBEDDED_JAVA}" ]]; then
    echo >&2 "Missing embedded JDK launcher: ${EMBEDDED_JAVA}"
    exit 1
  fi

  "${EMBEDDED_JAVA}" -jar "${PROGRAM}" --help
fi

echo "All good!"
