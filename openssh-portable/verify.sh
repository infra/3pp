# First and only positional argument is the path to the built cipd package file.
pkg="$1"

tempdir="$(mktemp -d)"
trap "rm -rf $tempdir" EXIT

cipd pkg-deploy "$pkg" -root "$tempdir"

ls -lahR $tempdir

if [[ ! -f "$tempdir/bin/scp" ]]; then
    echo "scp missing from package!"
    exit 1
fi


if [[ ! -f "$tempdir/bin/sftp" ]]; then
    echo "sftp missing from package!"
    exit 1
fi


if [[ ! -f "$tempdir/bin/ssh" ]]; then
    echo "ssh missing from package!"
    exit 1
fi


if [[ ! -f "$tempdir/bin/ssh-add" ]]; then
    echo "ssh-add missing from package!"
    exit 1
fi


if [[ ! -f "$tempdir/bin/ssh-agent" ]]; then
    echo "ssh-agent missing from package!"
    exit 1
fi


if [[ ! -f "$tempdir/bin/ssh-keygen" ]]; then
    echo "ssh-keygen missing from package!"
    exit 1
fi

exit 0