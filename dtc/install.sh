#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -euxo pipefail

PREFIX="$1"

PATH="$2/bin/:$PATH"
make NO_PYTHON="1" EXTRA_CFLAGS=" -Wno-error=sign-compare "
cp dtc $PREFIX
