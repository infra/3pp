create {
  platform_re: "(linux|mac)-(amd64|arm64)"
  source {
    cipd {
      pkg: "infra/3pp/tools/cpython3/${platform}"
      default_version: "2@3.8.10.chromium.26"
      original_download_url: "https://chrome-infra-packages.appspot.com/p/infra/3pp/tools/cpython3/${platform}/+/version:${version}"
    }
    patch_version: "2"
  }

  build {}

  package {
    version_file: ".versions/compact_python3.version"
  }
}

create {
  platform_re: "linux-amd64|mac-(amd64|arm64)"
  verify {
    test: "verify.sh"
  }
}

upload {}
