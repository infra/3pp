#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e
set -x
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Assume the binary's installation name is the recipe's directory name,
_DEFAULT_TOOL_NAME="$(basename "${SCRIPT_DIR}")"

# This script is normally called by the 3pp LUCI recipe, but can
# be tested locally provided that:
# - The _BAZEL_BIN env variable is set to point to downloaded binary,
# - The _3PP_VERSION env variable is set to the build version.
# - The script is invoked with the path of an installation prefix.
#
# e.g.:
#    mkdir -p /tmp/install
#    rm -rf /tmp/install/*
#    _3PP_VERSION=5.2.0 _BINARY=/path/to/<binary> ./install.sh /tmp/install

PREFIX="$1"

# Path to the source binary.
# This is normally 'raw_source_0' when the script is invoked
# from the 3pp recipe, but allow overriding it with _BAZEL_BIN
# for testing this script locally.
_BINARY="${_BINARY:-raw_source_0}"

# Set _3PP_VERSION to "unknown" for local testing. This will always be set
# from the LUCI recipe, and might include a CIPD patch level prefix,
# e.g. '5.2.0-fuchsia0' which would be incremented when this
# file is updated, but not the actual Bazel binaries themselves.
_3PP_VERSION="${_3PP_VERSION:-unknown}"

# The final tool name can be overriden for debugging this script.
_TOOL_NAME="${_TOOL_NAME:-"${_DEFAULT_TOOL_NAME}"}"

# Copy binary to the PREFIX directory with its final name.
cp "${_BINARY}" "${PREFIX}/${_TOOL_NAME}"
chmod 0750 "${PREFIX}/${_TOOL_NAME}"

cp "${SCRIPT_DIR}"/LICENSE "${PREFIX}"/LICENSE

# Generate README.fuchsia while injecting the right version.
sed -e 's|@VERSION@|'"${_3PP_VERSION}"'|g' \
    "${SCRIPT_DIR}"/README.fuchsia.template > "${PREFIX}/README.fuchsia"
