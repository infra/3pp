#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -exo pipefail

# This runs in a new empty directory, unpack the CIPD package to verify
# its content and that it runs properly.
unzip -q "$1"

# Dump current directory content for debugging
ls -l .

if [[ "$_3PP_PLATFORM" != "$_3PP_TOOL_PLATFORM" ]]; then
  echo "Cannot run tests on cross-compiled binary"
  exit 0
fi

# Run the 'bazel version' command into a new empty output_user_root.
# Note that `bash -x` is used to print the script's internal commands
# for debugging. And /bin/bash is called directly because ${SHELL}
# can point to /bin/sh.
"${BASH}" -x ./bazel --output_user_root=output_user_root version \
        2>&1 | tee stdout.txt

# Verify that the version contains a `Build label' value that matches
# _3PP_VERSION. Note that the latter may include a suffix like -fuchsia1
# corresponding to the version of the 3pp scripts, and not of the binary
# itself, and should be ignored.
BUILD_LABEL="$(awk '$1 == "Build" && $2 == "label:" { print $3; }' stdout.txt)"
if [[ -z "${BUILD_LABEL}" ]]; then
  echo >&2 "Could not find Bazel build label!"
  exit 1
fi
# Check that _3PP_VERSION starts with BUILD_LABEL.
if [[ "${_3PP_VERSION#"${BUILD_LABEL}"}" == "${_3PP_VERSION}" ]]; then
  echo >&2 "Invalid Bazel build label (${BUILD_LABEL}), does not match ${_3PP_VERSION}"
  exit 1
fi

# Verify that the output_user_root has no 'install' directory.
# This would be created if the --install_base argument is not given
# to the final Bazel binary.
if [[ -d "output_user_root/install" ]]; then
    echo "Unexpected extracted install directory:"
    ls output_user_root/install/
    exit 1
fi

# Verify that the stderr does not contain a line that starts with "Extracting"
# This would happen if the --output_user_root option was ignored, and the
# install_base was not used.
if grep -F "Extracting" stdout.txt; then
    echo "Unexpected extration of install_base:"
    cat stdout.txt
    exit 1
fi

echo "All good!"
