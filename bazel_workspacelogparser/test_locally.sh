#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
set -eo pipefail

# This script can be used to test the 'fetch.py', 'install.sh' and
# 'verify'sh' scripts locally without running the 3pp LUCI recipe.
# Usage:
#    ./test_locally.sh [BAZEL_BIN] [ARCHIVE]
#
# Where BAZEL_BIN is an optional path to a specific Bazel binary.
# Where ARCHIVE is an optional path to a pre-downloaded Bazel source archive.
#
# It will use the 'bazel' program in the current PATH.
#

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

die () {
  echo >&2 "ERROR: $*"
  exit 1
}

BAZEL=$1
SOURCE_ARCHIVE=$2

if [[ -z "${BAZEL}" ]]; then
  BAZEL=$(which bazel 2>/dev/null || echo "")
  if [[ -z "${BAZEL}" ]]; then
    die "A 'bazel' binary is required in your path."
  fi
fi

ROOT_BUILD_DIR="$(mktemp -d)"
echo "Root build dir: $ROOT_BUILD_DIR"

if [[ -z "${SOURCE_ARCHIVE}" ]]; then
  echo "Downloading latest Bazel source archive."
  LATEST=$("${SCRIPT_DIR}"/fetch.py latest)
  if [[ -z "${LATEST}" ]]; then
    die "Could not determine latest Bazel source version!"
  fi

  echo "Latest version: ${LATEST}"

  export _3PP_VERSION="${LATEST}"
  URL_MANIFEST="$("${SCRIPT_DIR}"/fetch.py get_url)"
  if [[ -z "${URL_MANIFEST}" ]]; then
    die "Could not determine latest Bazel source archive URL!"
  fi

  URL="$(echo "${URL_MANIFEST}" | jq .url[0] | sed -e 's|"||g')"
  echo "URL Manifest: ${URL_MANIFEST}"
  echo "URL: ${URL}"
  SOURCE_ARCHIVE=bazel.src.tar.gz
  curl --output-dir "${ROOT_BUILD_DIR}" --output "${SOURCE_ARCHIVE}" --location "${URL}"

  SOURCE_ARCHIVE="${ROOT_BUILD_DIR}/${SOURCE_ARCHIVE}"
else
  echo "Using cached Bazel source archive: ${SOURCE_ARCHIVE}"
fi

SOURCE_DIR="${ROOT_BUILD_DIR}"/source
mkdir -p "${SOURCE_DIR}"
tar -xf "${SOURCE_ARCHIVE}" -C "${SOURCE_DIR}" --strip-components=1

INSTALL_DIR="${ROOT_BUILD_DIR}/install"
mkdir -p "${INSTALL_DIR}"

VERIFY_DIR="${ROOT_BUILD_DIR}/verify"
mkdir -p "${VERIFY_DIR}"

# Build archive content
(
  cd "${SOURCE_DIR}" &&
  _3PP_VERSION="${BAZEL_VERSION}" \
  _BAZEL_BIN="$BAZEL" \
    "$SCRIPT_DIR/install.sh" "${INSTALL_DIR}")

# Create archive for verification
echo -n "Creating compressed archive for verification..."
ARCHIVE="${ROOT_BUILD_DIR}/archive.zip"
(cd "${INSTALL_DIR}" && zip -q -0r "${ARCHIVE}" ./*)
echo ""

# Verify archive content
(
  cd "${VERIFY_DIR}" &&
  _3PP_VERSION="${BAZEL_VERSION}" \
  _BAZEL_BIN="${BAZEL}" \
  "${SCRIPT_DIR}"/verify.sh "${ARCHIVE}")
