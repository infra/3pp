#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e
set -x
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# This script is normally called by the 3pp LUCI recipe, but can
# be tested locally provided that:
# - The _BAZEL_BIN env variable is set to point to a real bazel binary.
# - The _3PP_VERSION env variable is set to the Bazel build label.
# - The script is invoked with the path of an installation prefix.
#
# e.g.:
#    mkdir -p /tmp/bazel-install
#    rm -rf /mtp/bazel-install/*
#    _3PP_VERSION=5.2.0 _BAZEL_BIN=/usr/bin/bazel-real ./install.sh /tmp/bazel-install

PREFIX="$1"

# Path to the source Bazel binary.
# This is normally 'raw_source_0' when the script is invoked
# from the 3pp recipe, but allow overriding it with _BAZEL_BIN
# for testing this script locally.
_BAZEL_BIN="${_BAZEL_BIN:-raw_source_0}"

# Set _3PP_VERSION to "unknown" for local testing. This will always be set
# from the LUCI recipe, and might include a CIPD patch level prefix,
# e.g. '5.2.0-fuchsia0' which would be incremented when this
# file is updated, but not the actual Bazel binaries themselves.
_3PP_VERSION="${_3PP_VERSION:-unknown}"

# The Bazel binary is at `raw_source_0` and is also a Zip archive.
# Unpack it directly, which is considerably faster than trying to run
# the program with a temp output_user_dir.
INSTALL_BASE="${PREFIX}/install_base"
mkdir "${INSTALL_BASE}"
unzip -d "${INSTALL_BASE}" "${_BAZEL_BIN}"

# Bazel unpacks all files with mode 750, so enforce that here too.
chmod -R 0750 "${INSTALL_BASE}"

# Bazel requires all timestamps in the install_base to be 10 years in
# the future (this is a cheap way to check for integrity). However timestamps
# are not preserved in the CIPD archive uploaded by the recipe (or maybe
# by CIPD during extraction?), so do not try to adjust them here, this
# will be handled by the wrapper script.

# Copy the wrapper script, which expects the real Bazel binary to be
# named `bazel-real` (this matches Linux Debian), and the install_base
# directory to be named `install_base`.
cp "${SCRIPT_DIR}"/bazel_wrapper_script.bash "${PREFIX}/bazel"

cp "${SCRIPT_DIR}"/bazel.LICENSE "${PREFIX}/LICENSE"

cp "${_BAZEL_BIN}" "${PREFIX}/bazel-real"
chmod a+x "${PREFIX}/bazel-real" "${PREFIX}/bazel"

# Generate README.fuchsia while injecting the right version.
sed -e 's|@BAZEL_VERSION@|'"${_3PP_VERSION}"'|g' \
    "${SCRIPT_DIR}"/README.fuchsia.template > "${PREFIX}/README.fuchsia"

