#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -euxo pipefail

PREFIX="$1"
DEPS="$2"

CFLAGS="-fPIC"
LDFLAGS="-L$DEPS/lib/ $LDFLAGS -pthread -lpthread"

autoreconf

if [ "$_3PP_PLATFORM" == linux-arm64 ] ; then
    arch=arm
else
    arch=x86_64
fi

# This install is only supported on Linux -- enforced by platform_re in 3pp.pb
os=linux

./configure --host=$arch-$os --prefix="$PREFIX" --with-ssl-dir=$DEPS --with-zlib=$DEPS --disable-strip

make && make install-nokeys
