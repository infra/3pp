#!/usr/bin/env bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -eu -o pipefail

if [[ "$_3PP_PLATFORM" != "$_3PP_TOOL_PLATFORM" ]]; then
  echo "Cannot run tests on cross-compiled binary"
  exit 0
fi

# First and only positional argument is the path to the built cipd package file.
pkg="$1"

tempdir="$(mktemp -d)"
trap "rm -rf $tempdir" EXIT

cipd pkg-deploy "$pkg" -root "$tempdir"

keep_sorted="$tempdir/keep-sorted"

unsorted="
# keep-sorted start
b
e
a
d
c
# keep-sorted end
"
echo "$unsorted" > "$tempdir/unsorted.txt"

sorted="
# keep-sorted start
a
b
c
d
e
# keep-sorted end
"
echo "$sorted" > "$tempdir/sorted.txt"

if "$keep_sorted" --mode=lint "$tempdir/unsorted.txt"; then
  echo "Expected keep-sorted to fail on unsorted file"
  exit 1
fi

if ! "$keep_sorted" --mode=lint "$tempdir/sorted.txt"; then
  echo "Expected keep-sorted to pass on sorted file"
  exit 1
fi

"$keep_sorted" --mode=fix "$tempdir/unsorted.txt"

if ! diff "$tempdir/unsorted.txt" "$tempdir/sorted.txt"; then
  echo "Expected keep-sorted to sort unsorted.txt"
  exit 1
fi

echo
echo "All checks passed!"
