#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -euxo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

PREFIX="$1"

# Set _3PP_VERSION to "unknown" for local testing. This will always be set
# from the LUCI recipe, and might include a CIPD patch level prefix,
# e.g. '5.2.0-fuchsia0' which would be incremented when this
# file is updated, but not the actual tool binaries themselves.
_3PP_VERSION="${_3PP_VERSION:-unknown}"

# VPATH configure/make-ing is broken, so we must build in the srcdir.
#   * The configure tests for AC_CHECK_PROTOTYPE_LIB all fail because they
#     cannot find "${top_srcdir}/sysincludes.h".
# mkdir build
# cd build

# configure script issues:
#   * configure.ac has some scripts with underquoted [ ] (at configure:5224)
./configure --prefix="$PREFIX" --disable-readline -C

make -j "$(nproc)"

# Tests don't all pass.
#   * Some tests fail because they assume ${builddir} == ${srcdir}.
# ${top_srcdir}/test.sh

make install

cp COPYING "${PREFIX}"/LICENSE

# Generate README.fuchsia while injecting the right version.
sed -e "s|@VERSION@|${_3PP_VERSION}|g" \
    "${SCRIPT_DIR}"/README.fuchsia.template > "${PREFIX}/README.fuchsia"
