#!/usr/bin/env bash
# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -eu -o pipefail

PREFIX="$1"

cp raw_source_0 "${PREFIX}/bazel"
chmod a+x "${PREFIX}/bazel"

cp raw_source_0 "${PREFIX}/bazelisk"
chmod a+x "${PREFIX}/bazelisk"
