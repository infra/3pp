# First and only positional argument is the path to the built cipd package file.
pkg="$1"

tempdir="$(mktemp -d)"
trap "rm -rf $tempdir" EXIT

cipd pkg-deploy "$pkg" -root "$tempdir"

if ![ -f "$tempdir/dtc" ]; then
    echo "dtc binary missing from package!"
    exit 1
fi

exit 0