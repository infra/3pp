#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -euxo pipefail

PREFIX="$1"
DEPS="$2"

make CFLAGS=-I$DEPS/include LDFLAGS=-L$DEPS/lib
cp pigz "$PREFIX"
