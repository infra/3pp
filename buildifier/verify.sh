#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -exo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TOOL="$(basename "${SCRIPT_DIR}")"
TOOL_AWK_COMMAND='$2 == "version:" { print $3; }'

# This runs in a new empty directory, unpack the CIPD package to verify
# its content and that it runs properly.
unzip -q "$1"

# Run the tool binary and extract its version to verify it matches
# _3PP_VERSION. Note that the latter might have a suffix like -fuchsia1
# that should be ignored.
./"$TOOL" --version | tee stdout.txt
VERSION="$(awk "${TOOL_AWK_COMMAND}" stdout.txt)"
if [[ -z "${VERSION}" ]]; then
  echo >&2 "Could not find $TOOL version!"
  exit 1
fi

# The version computed by fetch.py is something like `v6.1.2` extracted
# from the official release download pages, but the tool now reports
# a version without the 'v' prefix (e.g. 6.1.2). Support both formats
# just in case:
VERSION_MATCHES=
if [[ "${_3PP_VERSION}" == "${VERSION}" ]]; then
  # Exact match
  VERSION_MATCHES=true
elif [[ "${_3PP_VERSION#v}" == "${VERSION}" ]]; then
  # _3PP_VERSION includes a v prefix, but matches the version otherwise.
  VERSION_MATCHES=true
fi

if [[ -z "${VERSION_MATCHES}" ]]; then
  echo >&2 "Invalid ${TOOL} version (${VERSION}), does not match ${_3PP_VERSION}"
  exit 1
fi

echo "All good!"
