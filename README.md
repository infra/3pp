Fuchsia Infra Third Party Packages (infra/3pp)
=======================================

This repository is for third party package definitions to assemble and upload
prebuilts to (CIPD) for consumption in fuchsia.git or for other Fuchsia
infrastructure services.


### Host source for new packages

Source for all packages must be mirrored and hosted internally at
fuchsia.googlesource.com for security.

To request that a source be added to fuchsia.googlesource.com, you should:

1) [File a ticket with the Open Source Review Board](https://bugs.fuchsia.dev/p/fuchsia/issues/entry?template=Open+Source+Review+Board+) for approval to host the desired source.

2) Once the source has been approved,
[File a ticket with Foundation Infra](http://go/fuchsia-new-repo-request)
to add the repository and set up mirroring.

### Add new package configurations

To dive right into the source to understand how Chrome's 3pp recipe builds
packages, follow the links below:

[Documentation outlining how Chrome's 3pp recipe builds packages](https://chromium.googlesource.com/infra/infra/+/HEAD/recipes/recipe_modules/support_3pp/api.py#18)

[Protobuf definitions and documentation for 3pp.pb files](https://chromium.googlesource.com/infra/infra/+/HEAD/recipes/recipe_modules/support_3pp/spec.proto)

For a more hands-on approach to learning, create a change adding a new subfolder
named after whatever package you're trying to build and add a `3pp.pb` file to
that subfolder with the following contents, substituting contents in wickets <>
where appropriate. 

Upload your change to Gerrit and use the 3pp presubmit builders to test building
your package. Delete all informational comments prior to submitting your change
for review.

```
create {
    # Regular expression for target os+cpu architecture
    # This example resolves to linux-amd64, linux-arm64, mac-amd64, mac-arm64.
    platform_re: "(linux|mac)-(amd64|arm64)"

    # source options
    # git            Specify a git repository to pull the source from.
    # patch_version  Specify an additional patch to be appended to the version
    #                tag. This option is commonly used to build new versions of
    #                the same source tag by incrementing the patch count.
    #
    # Note: New packages should only use `git` as source and should point to 
    # fuchsia.googlesource.com repositories.
    # Older packages will be migrated in short order.
    source {
        # git options
        # repo         A url pointing at a git repository hosted at 
        #              fuchsia.googlesource.com.
        # tag_pattern  A string formatting pattern to identify tag versions.
        #              the 3pp recipe only builds the most latest tagged version
        #              and ignores any packages that already have built versions
        #              latest tag.
        git {
            repo: "https://fuchsia.googlesource.com/<your_source_directory>"
            tag_pattern: "v%s" # e.x. v1.2.3 -> 1.2.3
        }
    }

    # build options
    # install       The script that will execute to transform your source into
    #               the desired output.
    # dep           A package that will be built by 3pp first and used as input
    #               to this packages build scripts. The OS path for these 
    #               dependencies is passed into the build script as $2.
    # external_dep  Similar to dep except it imports an arbitrary CIPD package.
    build {
        install: "install.sh"
        dep: "<other-3pp-package>"
        external_dep: "path/to/external/package${platform}@<revision>"
    }

    # verify options
    # test     A script to verify that the package was created properly.
    #          The built package will be zipped and passed to the script as $1.
    verify {
        test: [ "verify.sh" ]
    }
}

# upload options
# pkg_prefix            A prefix to prepend to the uploaded package path. The
#                       root path is `fuchsia/third_party/`, so if pkg_prefix
#                       were set to "source", the full path would be
#                       `fuchsia/third_party/source/`.
# pkg_name_override     Overrides the name the package will be uploaded as. The
#                       default value is the directory name.
upload {
    pkg_prefix: ""
    pkg_name_override: "<your-package>"
}
```

### Add an OWNERS file for your package

Once your package has been set up, add an OWNERS file containing the intended
owners and maintainers of your package. Broken packages will have bugs filed
against the listed users.