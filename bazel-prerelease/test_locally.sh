#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
set -eo pipefail

# This script can be used to test the 'install.sh' and 'verify'sh'
# scripts locally without running the 3pp LUCI recipe.
# Usage:
#    ./test_locally.sh <BAZEL_BIN> <BAZEL_VERSION>
#
# Where <BAZEL_BIN> points to the real Bazel binary (note that 'bazel'
# can be a wrapper script to the real binary, e.g. it would be
# /usr/bin/bazel-real on a Debian based system).
#
# Where <BAZEL_VERSION> is the Bazel build label for said binary.
# (which can be obtained with `bazel info`
#
# E.g.:
#    ./test_locally.sh /usr/bin/bazel-real 5.2.0

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BAZEL_BIN=$1
BAZEL_VERSION=$2

die () {
  echo >&2 "ERROR: $@"
  exit 1
}

if [[ -z "${BAZEL_BIN}" ]]; then
  die "Expected path to Bazel binary as first argument."
fi
if [[ -z "${BAZEL_VERSION}" ]]; then
  die "Expected bazel version string as second argument."
fi

ROOT_BUILD_DIR="$(mktemp -d)"
INSTALL_DIR="${ROOT_BUILD_DIR}/install"
mkdir -p "${INSTALL_DIR}"

VERIFY_DIR="${ROOT_BUILD_DIR}/verify"
mkdir -p "${VERIFY_DIR}"

# Build archive content
_3PP_VERSION="${BAZEL_VERSION}" \
_BAZEL_BIN="${BAZEL_BIN}" \
  "$SCRIPT_DIR/install.sh" "${INSTALL_DIR}"

# Create archive for verification
echo -n "Creating compressed archive for verification..."
ARCHIVE="${ROOT_BUILD_DIR}/archive.zip"
(cd "${INSTALL_DIR}" && zip -q -0r "${ARCHIVE}" *)
echo ""

# Verify archive content
(cd "${VERIFY_DIR}" && _3PP_VERSION="${BAZEL_VERSION}" "${SCRIPT_DIR}"/verify.sh "${ARCHIVE}")
