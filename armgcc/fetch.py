#!/usr/bin/env python
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import dataclasses
import json
import os
import re
import sys
import urllib.request

# Check for new versions at the following URL:
# https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads
# Change this line when a new version is released.
_VERSION = '13.3.rel1'

# Often this line needs to be changed with new versions as well.
_URL = 'https://developer.arm.com/-/media/Files/downloads/gnu/{version}/binrel/arm-gnu-toolchain-{version}-{platform}-arm-none-eabi{ext}'


def do_latest():
    """Retrieve current version.

    It would be overly complicated to get the latest version, so we just use a
    fixed version. When a new version is released the line at the top will need
    to be updated.
    """
    print(_VERSION)
    return 0


def get_download_url(version, platform):
    """Get URL of the given version and platform."""

    platform_part = {
        'linux-amd64': 'x86_64',
        'linux-arm64': 'aarch64',
        'mac-amd64': 'darwin-x86_64',
        'mac-arm64': 'darwin-arm64',
        'windows-amd64': 'mingw-w64-i686',
    }[platform]

    ext = '.zip' if 'win' in platform else '.tar.xz'

    url = _URL.format(version=version, platform=platform_part, ext=ext)

    manifest = {'url': [url], 'ext': ext}
    print(json.dumps(manifest))


def main():
  ap = argparse.ArgumentParser()
  sub = ap.add_subparsers()

  latest = sub.add_parser("latest")
  latest.set_defaults(func=lambda _opts: do_latest())

  download = sub.add_parser("get_url")
  download.set_defaults(
    func=lambda opts: get_download_url(
      os.environ['_3PP_VERSION'], os.environ['_3PP_PLATFORM']
    )
  )

  opts = ap.parse_args()
  return opts.func(opts)


if __name__ == '__main__':
    sys.exit(main())
