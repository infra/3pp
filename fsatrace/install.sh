#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -euxo pipefail

PREFIX="$1"

make

cp fsatrace "$PREFIX"
cp fsatrace.so "$PREFIX"
cp LICENSE "$PREFIX"
