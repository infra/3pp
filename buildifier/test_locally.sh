#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
set -x
set -eo pipefail

# This script can be used to test the 'install.sh' and 'verify'sh'
# scripts locally without running the 3pp LUCI recipe.
# Usage:
#    ./test_locally.sh <BINARY> [<VERSION>]
#
# Where <BINARY> points to the real tool binary..
#
# Where <VERSION> is the 3pp version for said binary,
# (extracted from `<tool> --version` by default).
#
# E.g.:
#    # Download latest binary
#    ./test_locall.sh
#
#    # Test a given binary download on the local machine
#    ./test_locally.sh /path/to/buildifier-linux-amd64
#
#    # Check with a custom _3PP_VERSION value
#    ./test_locally.sh /path/to/buildifier-linux-amd64 5.1.0-fuchsia1
#
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BINARY=$1
VERSION=$2

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
INSTALL_NAME="$(basename "${SCRIPT_DIR}")"

die () {
  echo >&2 "ERROR: $@"
  exit 1
}

ROOT_BUILD_DIR="$(mktemp -d)"

if [[ -z "${BINARY}" ]]; then
  VERSION="$(python "${SCRIPT_DIR}"/fetch.py latest)"
  if [[ -z "${VERSION}" ]]; then
    die "Cannot get latest version for $TOOL_NAME! (check fetch.py!)"
  fi
  echo "Latest release: ${VERSION}"
  DOWNLOAD_URL_JSON="$(_3PP_VERSION="${VERSION}" _3PP_PLATFORM="linux-amd64" python "${SCRIPT_DIR}"/fetch.py get_url)"
  URL="$(echo -n "${DOWNLOAD_URL_JSON}" | awk '{ print $2; }'| sed -e 's/\["//g' -e 's/"]//g' -e 's/,//g')"
  echo "Downlad URL: ${URL}"
  BINARY="${ROOT_BUILD_DIR}/${INSTALL_NAME}-${VERSION}"
  curl --output "${BINARY}" --location "${URL}"
fi
if [[ -z "${VERSION}" ]]; then
  VERSION="$("${BINARY}" --version|awk '$2 == "version:" { print $3; }')"
  if [[ -z "${VERSION}" ]]; then
    die "Could not guess version from: $BINARY"
  fi
  echo "Using version: $VERSION"
fi

INSTALL_DIR="${ROOT_BUILD_DIR}/install"
mkdir -p "${INSTALL_DIR}"

VERIFY_DIR="${ROOT_BUILD_DIR}/verify"
mkdir -p "${VERIFY_DIR}"

# Build archive content
_3PP_VERSION="${VERSION}" \
_BINARY="${BINARY}" \
  "$SCRIPT_DIR/install.sh" "${INSTALL_DIR}"

# Create archive for verification
echo -n "Creating compressed archive for verification..."
ARCHIVE="${ROOT_BUILD_DIR}/archive.zip"
(cd "${INSTALL_DIR}" && zip -q -0r "${ARCHIVE}" *)
echo ""

# Verify archive content
(cd "${VERIFY_DIR}" && _3PP_VERSION="${VERSION}" "${SCRIPT_DIR}"/verify.sh "${ARCHIVE}")
