#!/usr/bin/env bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -eu -o pipefail

# First and only positional argument is the path to the built cipd package file.
pkg="$1"

tempdir="$(mktemp -d)"
trap "rm -rf $tempdir" EXIT

cipd pkg-deploy "$pkg" -root "$tempdir"

# TODO(olivernewman): The black prebuilts dynamically link against a version of
# glibc that isn't available on the docker images used for verification by 3pp.
if [[ "$(uname -s)" == "Linux" ]]; then
  echo "Skipping verification on Linux due to potential glibc incompatibility"
  exit 0
fi

# mac-arm64 prebuilt binary requires Rosetta to run because it is an x64 binary,
# but Rosetta is not guaranteed to be available on infra bots.
if [[ "$(uname -s)" == "Darwin" &&  "$(uname -m)" == "arm64" ]]; then
  echo "Skipping verification on mac-arm64 due to the dependency on Rosetta"
  exit 0
fi

black="$tempdir/black"

unformatted="
def func(  bar, baz) :
  print(
'I am not formatted correctly :(')
"
echo "$unformatted" > "$tempdir/unformatted.py"

formatted='def func(bar, baz):
    print("I am formatted correctly!")'
echo "$formatted" > "$tempdir/formatted.py"

if ! "$black" --check --diff "$tempdir/formatted.py"; then
  echo "Expected black --check to pass on a formatted file"
  exit 1
fi

if "$black" --check "$tempdir/unformatted.py"; then
  echo "Expected black --check to fail on a badly formatted file"
  exit 1
fi

echo
echo "All checks passed!"
