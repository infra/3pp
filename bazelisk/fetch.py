#!/usr/bin/env python
# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import json
import os
import sys
import urllib.request


def do_latest():
    metadata_url = "https://api.github.com/repos/bazelbuild/bazelisk/releases/latest"
    resp = json.load(urllib.request.urlopen(metadata_url))
    print(resp["tag_name"].lstrip("v"))


def get_download_url(version, platform):
    os_name, arch = platform.split("-")
    os_name = os_name.replace("mac", "darwin")
    ext = ".exe" if platform.startswith("windows") else ""

    name = f"bazelisk-{os_name}-{arch}{ext}"
    url = f"https://github.com/bazelbuild/bazelisk/releases/download/v{version}/{name}"

    partial_manifest = {
        "url": [url],
        "ext": ext,
    }
    print(json.dumps(partial_manifest))


def main():
    ap = argparse.ArgumentParser()

    sub = ap.add_subparsers()

    latest = sub.add_parser("latest")
    latest.set_defaults(func=lambda _opts: do_latest())

    download = sub.add_parser("get_url")
    download.set_defaults(
        func=lambda opts: get_download_url(
            os.environ["_3PP_VERSION"], os.environ["_3PP_PLATFORM"]
        )
    )

    opts = ap.parse_args()
    return opts.func(opts)


if __name__ == "__main__":
    sys.exit(main())
