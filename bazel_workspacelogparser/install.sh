#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e
set -x
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# This script is normally called by the 3pp LUCI recipe, but can
# be tested locally provided that:
# - The _BAZEL_BIN env variable is set to point to a real bazel binary.
# - The _3PP_VERSION env variable is set to the Bazel build label.
# - The script is invoked with the path of an installation prefix.
#
# e.g.:
#    mkdir -p /tmp/bazel-install
#    rm -rf /mtp/bazel-install/*
#    _3PP_VERSION=5.2.0 _BAZEL_BIN=/usr/bin/bazel-real ./install.sh /tmp/bazel-install

INSTALL_PREFIX="$1"

# Path to the source Bazel binary.
# This is normally in our PATH due to the use of build.tool in 3pp.pb
# but can be overriden by setting _BAZEL_BIN in the environment.
if [[ -z "${_BAZEL_BIN}" ]]; then
  _BAZEL_BIN="$(which bazel)"
  if [[ ! -f "${_BAZEL_BIN}" ]]; then
    echo >&2 "ERROR: No 'bazel' in PATH!"
    exit 1
  fi
fi

# Set _3PP_VERSION to "unknown" for local testing. This will always be set
# from the LUCI recipe, and might include a CIPD patch level prefix,
# e.g. '5.2.0-fuchsia0' which would be incremented when this
# file is updated, but not the actual Bazel binaries themselves.
_3PP_VERSION="${_3PP_VERSION:-unknown}"

# Build the parser as a single .jar for deployment, then rename it as
# bazel_workspacelogparser.jar. The --java_runtime_version=... flag is
# used to download a JDK since none will be installed when running the
# recipe in a Docker container.
#
# Note that the embedded JDK that comes with the Bazel installation
# cannot be used. Bazel will complain that it is missing critical
# files and considers it to be a JRE instead.
"${_BAZEL_BIN}" build \
  --java_runtime_version=remotejdk_11  \
  //src/tools/workspacelog:parser_deploy.jar

cp bazel-bin/src/tools/workspacelog/parser_deploy.jar \
   "${INSTALL_PREFIX}/bazel_workspacelogparser.jar"

# Bazel requires all timestamps in the install_base to be 10 years in
# the future (this is a cheap way to check for integrity). However timestamps
# are not preserved in the CIPD archive uploaded by the recipe (or maybe
# by CIPD during extraction?), so do not try to adjust them here, this
# will be handled by the wrapper script.

# Copy the Bazel license.
cp "${SCRIPT_DIR}"/bazel.LICENSE "${INSTALL_PREFIX}/LICENSE"

# Generate README.fuchsia while injecting the right version.
sed -e 's|@BAZEL_VERSION@|'"${_3PP_VERSION}"'|g' \
    "${SCRIPT_DIR}"/README.fuchsia.template > "${INSTALL_PREFIX}/README.fuchsia"
