#!/usr/bin/env python
# Copyright 2022 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import json
import os
import re
import urllib.request

_TOOL_NAME = os.path.basename(os.path.dirname(__file__))


def do_latest():
    print(
        json.load(
            urllib.request.urlopen(
                "https://api.github.com/repos/astral-sh/ruff/releases/latest"
            )
        )["tag_name"]
    )


_PLATFORMS = {
    "linux-amd64": "x86_64-unknown-linux-gnu",
    "linux-arm64": "aarch64-unknown-linux-gnu",
    "mac-amd64": "x86_64-apple-darwin",
    "mac-arm64": "aarch64-apple-darwin",
    "windows-amd64": "i686-pc-windows-msvc",
}

_EXTENSION = {
    "linux": ".tar.gz",
    "mac": ".tar.gz",
    "windows": ".zip",
}


def get_download_url(version, platform):
    if platform not in _PLATFORMS:
        raise ValueError("unsupported platform {}".format(platform))

    extension = _EXTENSION[platform.split("-")[0]]

    url = (
        "https://github.com/astral-sh/ruff/releases/download/{version}/"
        "{tool_name}-{platform}{extension}".format(
            tool_name=_TOOL_NAME,
            version=version,
            platform=_PLATFORMS[platform],
            extension=extension,
        )
    )

    manifest = {
        "url": [url],
        "ext": extension,
    }

    print(json.dumps(manifest))


# The following works with Python2 and Python3 as well for local testing.
_FUNCTION_MAP = {
    "latest": lambda: do_latest(),
    "get_url": lambda: get_download_url(
        os.environ["_3PP_VERSION"], os.environ["_3PP_PLATFORM"]
    ),
}


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("command", choices=_FUNCTION_MAP.keys())

    opts = ap.parse_args()
    _FUNCTION_MAP[opts.command]()


if __name__ == "__main__":
    main()
