create {
  platform_re: "linux-amd64"
  source {
    script { name: "fetch.py" }
    unpack_archive: true
    patch_version: "1"
  }

  build {
    external_dep: "infra/3pp/tools/autoconf/${platform}@2@2.69.chromium1"
    external_dep: "infra/3pp/tools/automake/${platform}@2@1.15.chromium1"
    external_tool: "infra/3pp/tools/gettext/${platform}@2@0.19.8.chromium.1"
  }
}

upload {}
