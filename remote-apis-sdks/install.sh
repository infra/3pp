#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e
set -x
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# The working directory is expected to start in the source checkout.

# This script is normally called by the 3pp LUCI recipe, but can
# be tested locally provided that:
# - The _BAZEL_BIN env variable is set to point to a real bazel binary.
# - The _3PP_VERSION env variable is set to the Bazel build label.
# - The script is invoked with the path of an installation prefix.
#
# e.g.:
#    mkdir -p /tmp/bazel-install
#    rm -rf /tmp/bazel-install/*
#    _3PP_VERSION=5.2.0 _BAZEL_BIN=/usr/bin/bazel-real ./install.sh /tmp/bazel-install

PREFIX="$1"

# Path to the source Bazel binary.
# This is normally 'bazel' when the script is invoked
# from the 3pp recipe, but allow overriding it with _BAZEL_BIN
# for testing this script locally.
_BAZEL_BIN="${_BAZEL_BIN:-bazel}"

# Set _3PP_VERSION to "unknown" for local testing. This will always be set
# from the LUCI recipe, and might include a CIPD patch level prefix,
# e.g. '5.2.0-fuchsia0' which would be incremented when this
# file is updated, but not the actual installed binaries themselves.
_3PP_VERSION="${_3PP_VERSION:-unknown}"

# Build it:
"${_BAZEL_BIN}" build ...

# Install the CLI tools:
cp bazel-bin/go/cmd/remotetool/remotetool_/remotetool "${PREFIX}"/
cp bazel-bin/go/cmd/rexec/rexec_/rexec "${PREFIX}"/

# Copy over other necessary files.
cp LICENSE "${PREFIX}/LICENSE"

# Generate README.fuchsia while injecting the right version.
sed -e 's|@VERSION@|'"${_3PP_VERSION}"'|g' \
    "${SCRIPT_DIR}"/README.fuchsia.template > "${PREFIX}/README.fuchsia"

