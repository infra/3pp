#!/bin/sh
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# First and only positional argument is the path to the built cipd package file.
pkg="$1"

tempdir="$(mktemp -d)"
trap "rm -rf $tempdir" EXIT

cipd pkg-deploy "$pkg" -root "$tempdir"

cat > hello.py <<EOF
import sys
if __name__ == "__main__":
  print("Hello Python!")
  print("System path: %s" % sys.path)

EOF

"$tempdir"/bin/python3 hello.py

# The include directory is also required by the Fuchsia build ow
if ! [ -d "$tempdir/include" ]; then
  echo >&2 "Missing include directory!"
  exit 1
fi

