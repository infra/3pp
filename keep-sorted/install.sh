#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -eux -o pipefail

PREFIX="$1"
DEPS="$2"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

"$DEPS/bin/go" build -o "${PREFIX}/keep-sorted" .

# Copy over license file.
cp LICENSE "${PREFIX}/LICENSE"

# Generate README.fuchsia while injecting the right version.
sed -e 's|@VERSION@|'"${_3PP_VERSION}"'|g' \
    "${SCRIPT_DIR}"/README.fuchsia.template > "${PREFIX}/README.fuchsia"
