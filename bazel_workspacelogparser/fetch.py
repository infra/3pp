#!/usr/bin/env python
# Copyright 2022 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import json
import os
import re
import urllib.request

# Set this to True to download the latest pre-releae verision of Bazel,
# instead of the latest relesse.
_USE_LATEST_PRERELEASE = True

_RE_NATURAL_ITEMS = re.compile("[^\W\d_]|\d+")


def natural_sort_key(item):
    """Convert a version number to a sorting key for natural ordering."""
    # Split the input item into a list of words and numbers (ignore everything else).
    # E.g. "6.0-pre2022:1" -> ("6", "0", "pre", "2022", "1")
    tups = _RE_NATURAL_ITEMS.findall(item)
    # Convert all numbers to their integer representation in the output list, but
    # keep words as is. For example (6, 0, "pre", 2022, 1)
    # Python will naturally sort tuple items in ascending order.
    return tuple(int(tup) if tup.isdigit() else tup for tup in tups)


def do_latest():
    """Retrieve tag of the latest pre-release to fetch."""
    if _USE_LATEST_PRERELEASE:
        # Pre-release versions are needed for the Fuchsia SDK and platform build.
        # To solve this, download the full list of tags as a JSON array of objects,
        # whose 'name' field gives the tag. Sort by increasing natural order, and
        # keep the last one.
        tags = json.load(
            urllib.request.urlopen("https://api.github.com/repos/bazelbuild/bazel/tags")
        )
        tag_names = sorted([tag["name"] for tag in tags], key=natural_sort_key)
        assert len(tag_names) > 0, "Empty tags list?"
        print(tag_names[-1])
    else:
        # This codes get the latest official release tag directly instead.
        print(
            json.load(
                urllib.request.urlopen(
                    "https://api.github.com/repos/bazelbuild/bazel/releases/latest"
                )
            )["tag_name"]
        )


def get_download_url(version):
    extension = ".tar.gz"

    url = f"https://github.com/bazelbuild/bazel/archive/refs/tags/{version}{extension}"

    manifest = {
        "url": [url],
        "ext": extension,
    }

    print(json.dumps(manifest))


def main():
    ap = argparse.ArgumentParser()
    sub = ap.add_subparsers()

    latest = sub.add_parser("latest")
    latest.set_defaults(func=lambda _opts: do_latest())

    download = sub.add_parser("get_url")
    download.set_defaults(
        func=lambda opts: get_download_url(os.environ["_3PP_VERSION"])
    )

    opts = ap.parse_args()
    opts.func(opts)


if __name__ == "__main__":
    main()
