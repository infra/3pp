#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -euxo pipefail

PREFIX="$1"

ar -vx raw_source_0.deb
tar xvf control.tar.xz
tar xvf data.tar.xz

md5=`md5sum usr/bin/iasl`

if [ "$md5" != "519a0edc9c2a1775e78a68c6b091f5b5  usr/bin/iasl" ]; then
  echo "invalid md5sum for usr/bin/iasl"
  exit 1
fi

cp usr/bin/iasl $1
